//Khai báo thư viện express
const express = require("express");

//Khai báo mongoose
const mongoose = require("mongoose");


//Khai báo các model của mongoose
const productTypeModel = require("./app/modules/productTypesModel");
const productModel = require("./app/modules/productModules");
const customerModel = require("./app/modules/customerModules");
const orderModel = require("./app/modules/orderModules");
const orderdetailModel = require("./app/modules/orderDetailModules");

//Khai báo các router
const { productTypeRouter } = require("./app/routes/productTypeRouter");
const { productRouter } = require("./app/routes/productRouter");
const { customerRouter } = require("./app/routes/customerRouter");
const { orderRouter } = require("./app/routes/orderRouter");
const { orderDetailRouter } = require("./app/routes/orderdetailRouter");


//Khởi tạo app
const app = express();

//Khai báo cổng
const port = 8000;

//Khai báo dử dụng json
app.use(express.json());

//Kết nối CSDL ( sử dụng mongoose phiên bản 6x)
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h",(error) =>{
    if(error) throw error;
    console.log("Connect Successfully !");
});

const MiddleWareForAll = (req,res,next) =>{
    console.log("MiddleWare For All Router ,Time :" + new Date()  + "Method : " + req.method );
    next();
}

//MiddleWare Dùng chung cho tất cả các router
app.use(MiddleWareForAll);

app.use("/",productTypeRouter);
app.use("/",productRouter);
app.use("/",customerRouter);
app.use("/",orderRouter);
app.use("/",orderDetailRouter);

app.listen(port , () =>{
    console.log("App listening on port : " , port);
})