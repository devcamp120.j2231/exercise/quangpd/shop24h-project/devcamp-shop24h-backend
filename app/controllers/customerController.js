//Import thư viện mongoose
const mongoose = require("mongoose");

//Import Customer Model 
const customerModel = require("../modules/customerModules");

//Get all Customer
const getAllCustomer = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    customerModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All Customer Successfully !`,
                product : data
            })
        }
    })
}

//Get Customer By id 
const getCustomerById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let customerId = req.params.customerId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message :`Bad Request - Customer Id is not valid`
        })
    }

    //B3 : Xử lý và trả về 
    customerModel.findById(customerId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : 'Get customer By Id Successfully !',
                customer : data
            })
        }
    })
}

//Create Customer
const createCustomer = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!body.fullName){
        return res.status(400).json({
            message : `Bad Request - fullName is required !`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad Request - phone is required !`
        })
    }

    if(!body.email){
        return res.status(400).json({
            message : `Bad Request - email is required !`
        })
    }

    if(!body.address){
        return res.status(400).json({
            message : `Bad Request - address is required !`
        })
    }

    if(!body.city){
        return res.status(400).json({
            message : `Bad Request - city is required !`
        })
    }

    if(!body.country){
        return res.status(400).json({
            message : `Bad Request - country is required !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    let newCustomer = new customerModel({
        _id : mongoose.Types.ObjectId(),
        fullName : body.fullName,
        phone : body.phone,
        email : body.email,
        address : body.address,
        city : body.city,
        country : body.country
    });

    customerModel.create(newCustomer,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Customer Successfully !`,
                customer : data
            })
        }
    })

}

//Update Customer 
const updateCustomerById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let customerId = req.params.customerId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message :`Bad Request  - Customer Id is not valid`
        })
    }

    if(!body.fullName){
        return res.status(400).json({
            message : `Bad Request - fullName is required !`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad Request - phone is required !`
        })
    }

    if(!body.email){
        return res.status(400).json({
            message : `Bad Request - email is required !`
        })
    }

    if(!body.address){
        return res.status(400).json({
            message : `Bad Request - address is required !`
        })
    }

    if(!body.city){
        return res.status(400).json({
            message : `Bad Request - city is required !`
        })
    }

    if(!body.country){
        return res.status(400).json({
            message : `Bad Request - country is required !`
        })
    }


    //B3 : Xử lý và trả về kết quả
    let updateCustomer = new customerModel({
        fullName : body.fullName,
        phone : body.phone,
        email : body.email,
        address : body.address,
        city : body.city,
        country : body.country
    });

    customerModel.findByIdAndUpdate(customerId,updateCustomer,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update Customer Successfully !`,
                customer : data
            })
        }
    })
}

//Delete Customer By Id 
const deleteCustomerById = (req,res) =>{
    //B1: thu thập dữ liệu
    let customerId = req.params.customerId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message :`Bad Request - Customer Id is not valid`
        })
    }

    //B3 : xử lý và trả về kết quả 
    customerModel.findByIdAndDelete(customerId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).json({
                message : `Delete Customer Successfully !`,
                customer : data
            })
        }
    })
}
//Export 
module.exports = {getAllCustomer , getCustomerById, createCustomer , updateCustomerById , deleteCustomerById}