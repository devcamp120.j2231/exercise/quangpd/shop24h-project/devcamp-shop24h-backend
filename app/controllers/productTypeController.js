//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Import model productType
const productTypeModel = require("../modules/productTypesModel");

//Get All ProductType
const getAllProductType = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    productTypeModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All Product Type Successfully !`,
                productType : data
            })
        }
    })
}


//Get ProductType By Id 
const getProductTypeById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productTypeId = req.params.productTypeId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            message : `Bad Request - ProductType Id is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    productTypeModel.findById(productTypeId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Get ProductType By Id Successfully !`,
                productType : data
            })
        }
    })
}

//Create New ProductType
const createNewProductType = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message :`Bad Request - Name Is Required !`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    let newProductType = new productTypeModel({
        _id : mongoose.Types.ObjectId(),
        name : body.name,
        description : body.description
    });

    productTypeModel.create(newProductType,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Create New ProductType Successfully!`,
                productType : data
            })
        }
    })
}

//Update Product By Id 
const updateProductTypeById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productTypeId = req.params.productTypeId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            message :`Bad request - ProductType Id is not valid`
        })
    }

    if(!body.name){
        return res.status(400).json({
            message :`Bad Request - Name Is Required !`
        })
    }

    //B3 : xử lý và trả về dữ liệu
    let updateProductType = new productTypeModel({
        name : body.name,
        description : body.description
    });

    productTypeModel.findByIdAndUpdate(productTypeId,updateProductType,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update Product Type Successfully !`,
                productType : data
            })
        }
    })



}

//Delete Product Type By Id 
const deleteProductTypeById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productTypeId = req.params.productTypeId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            message : `Bad Request - Product Type id is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    productTypeModel.findByIdAndDelete(productTypeId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : ` Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(204).json({
                message :`Delete ProductType Successfully`,
                productType : data
                
            })
        }
    })
    
}
//Export thành module
module.exports = { getAllProductType , getProductTypeById , createNewProductType , updateProductTypeById , deleteProductTypeById};