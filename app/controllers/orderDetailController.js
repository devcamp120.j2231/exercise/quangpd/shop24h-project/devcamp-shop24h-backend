//Import thư viện mongoose
const mongoose = require("mongoose");


//Import orderModel 
const orderModel = require("../modules/orderModules");
//Import orderDetailModel 
const orderDetailModel = require("../modules/orderDetailModules");

//Get All Order
const getAllOrderDetail = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    orderDetailModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All OrderDetail Successfully !`,
                orders : data
            })
        }
    })
}

//Get Order By Id 
const getOrderDetailByid = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderDetailId = req.params.orderDetailId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return res.status(400).json({
            message :`Bad Request - OrderDetail Id is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    orderDetailModel.findById(orderDetailId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`get orderDetail by orderDetail id successfully !`,
                order : data
            })
        }
    })
}

//Update Order By Id 
const updateOrderDetailById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderDetailId = req.params.orderDetailId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return res.status(400).json({
            message :`Bad request - Order Id is not valid`
        })
    }

    if(!Number(body.quantity)){
        return res.status(400).json({
            message : `Bad request - cost is a number !`
        })
    }

    //B3 : xử lý và trả về kết quả 
    let updateOrderDetail = orderModel({
        product : body.product,
        quantity : body.quantity
    });

    orderModel.findByIdAndUpdate(orderDetailId,updateOrderDetail,(err,data)=>{
        if(err){
            return res.status(500).json({
                message : `Internal server error ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update OrderDetail By id successfully !`,
                order : data
            })
        }
    })
    
}

//Create new orderDetail
const createNewOrderDetailOfOrder = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message : `Bad request - order Id is not valid`
        })
    }

    if(!Number(body.quantity)){
        return res.status(400).json({
            message : `Bad request - quantity is a number !`
        })
    }

    //B3 : xử lý và trả về kết quả 
    let newOrderDetail = orderModel({
        _id : mongoose.Types.ObjectId(),
        product : body.product,
        quantity : body.quantity
    });

    orderDetailModel.create(newOrderDetail,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        orderModel.findByIdAndUpdate(orderId,{
            $push : {orderDetails : data._id}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error  !`
                })
            }else{
                return res.status(201).json({
                    message : `Create OrderDetail Successfully !`,
                    order : data
                })
            }
        }
        )
    })

}

//Get all order of customer
const getAllOrderDetailOfOrder = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - customerId is not valid`
        })
    }

    //B3 :Xử lý và trả về dữ liệu
    orderModel.findById(orderId).populate("orderDetails").exec((err,data) => {
        if(err){
            return res.status(500).json({
                message :`Internal server error :${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Get All OrderDetail of Order Successfully !`,
                orders : data.orderDetails
            })
        }
    })
}

//Delete Order By order Id 
const deleteOrderDetailById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;
    let orderDetailId = req.params.orderDetailId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - order Id is not valid`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return res.status(400).json({
            message :`Bad request - OrderDetail ID is not valid`
        })
    }


    //B3 : xử lý và trả về 
    orderDetailModel.findByIdAndDelete(orderDetailId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        orderModel.findByIdAndUpdate(orderId,{
            $pull : {orderDetails : orderDetailId}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error !`
                })
            }else{
                return res.status(204).send()
                   
            }
        }
        )
    })
}

//export thành module
module.exports = { getAllOrderDetail ,getOrderDetailByid , createNewOrderDetailOfOrder , updateOrderDetailById,getAllOrderDetailOfOrder ,deleteOrderDetailById}