//Import thư viện mongoose
const mongoose = require("mongoose");

//Import Customer Model
const customerModel = require("../modules/customerModules");

//Import orderMOdel 
const orderModel = require("../modules/orderModules");

//Get All Order
const getAllOrder = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    orderModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All Order Successfully !`,
                orders : data
            })
        }
    })
}

//Get Order By Id 
const getOrderByid = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad Request - Order Id is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    orderModel.findById(orderId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`get order by order id successfully !`,
                order : data
            })
        }
    })
}

//Update Order By Id 
const updateOrderById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let orderId = req.params.orderId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - Order Id is not valid`
        })
    }

    if(!Number(body.cost)){
        return res.status(400).json({
            message : `Bad request - cost is a number !`
        })
    }

    //B3 : xử lý và trả về kết quả 
    let updateOrder = orderModel({
        shippedDate : body.shippedDate,
        note : body.note,
        cost : body.cost
    });

    orderModel.findByIdAndUpdate(orderId,updateOrder,(err,data)=>{
        if(err){
            return res.status(500).json({
                message : `Internal server error ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update Order By id successfully !`,
                order : data
            })
        }
    })
    
}

//Create new order
const createNewOrderOfCustomer = (req,res) =>{
    //B1 : thu thập dữ liệu
    let customerId = req.params.customerId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message : `Bad request - customer Id is not valid`
        })
    }

    if(!Number(body.cost)){
        return res.status(400).json({
            message : `Bad request - cost is a number !`
        })
    }

    //B3 : xử lý và trả về kết quả 
    let newOrder = orderModel({
        _id : mongoose.Types.ObjectId(),
        shippedDate : body.shippedDate,
        note : body.note,
        cost : body.cost
    });

    orderModel.create(newOrder,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        customerModel.findByIdAndUpdate(customerId,{
            $push : {orders : data._id}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error  !`
                })
            }else{
                return res.status(201).json({
                    message : `Create Order Successfully !`,
                    order : data
                })
            }
        }
        )
    })

}

//Get all order of customer
const getAllOrderOfCustomer = (req,res) =>{
    //B1 : thu thập dữ liệu
    let customerId = req.params.customerId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message :`Bad request - customerId is not valid`
        })
    }

    //B3 :Xử lý và trả về dữ liệu
    customerModel.findById(customerId).populate("orders").exec((err,data) => {
        if(err){
            return res.status(500).json({
                message :`Internal server error :${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Get All Order of Customer Successfully !`,
                orders : data.orders
            })
        }
    })
}

//Delete Order By order Id 
const deleteOrderById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let customerId = req.params.customerId;
    let orderId = req.params.orderId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message :`Bad request - customer Id is not valid`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message :`Bad request - orderId  is not valid`
        })
    }


    //B3 : xử lý và trả về 
    orderModel.findByIdAndDelete(orderId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        customerModel.findByIdAndUpdate(customerId,{
            $pull : {orders : orderId}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error !`
                })
            }else{
                return res.status(204).send()
                   
            }
        }
        )
    })
}
//Export thành module

module.exports = {getAllOrder , getOrderByid , createNewOrderOfCustomer , getAllOrderOfCustomer , updateOrderById , deleteOrderById};