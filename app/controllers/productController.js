//Import thư viện mogoose
const mongoose = require("mongoose");

//Import model Product
const productModel = require("../modules/productModules");

//Get all product
const getAllProduct = (req,res) =>{
    //B1 : Thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    productModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All Product Successfully !`,
                product : data
            })
        }
    })
}

//Get Product By Id 
const getProductById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productId = req.params.productId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            message :`Bad Request - Product Id is not valid`
        })
    }

    //B3 : Xử lý và trả về 
    productModel.findById(productId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : 'Get Product By Id Successfully !',
                product : data
            })
        }
    })
}

//Create  New Product
const createNewProduct = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message : `Bad Request - name is required !`
        })
    }

    if(!body.imageUrl){
        return res.status(400).json({
            message : `Bad Request - imageUrl is required !`
        })
    }

    if(!body.buyPrice){
        return res.status(400).json({
            message : `Bad Request - buyPrice is required !`
        })
    }

    if(!Number(body.buyPrice)){
        return res.status(400).json({
            message : `Bad Request - buyPrice is a number !`
        })
    }

    if(!body.promotionPrice){
        return res.status(400).json({
            message : `Bad Request - promotionPrice is required !`
        })
    }

    if(!Number(body.promotionPrice)){
        return res.status(400).json({
            message : `Bad Request - promotionPrice is a number !`
        })
    }

    if(!Number(body.amount)){
        return res.status(400).json({
            message : `Bad Request - amount is number !`
        })
    }


    //B3 : Xử lý và trả về kết quả 
    let newProduct = new productModel({
        _id : mongoose.Types.ObjectId(),
        name : body.name,
        description : body.description,
        imageUrl : body.imageUrl,
        buyPrice : body.buyPrice,
        promotionPrice : body.promotionPrice,
        amount : body.amount
    });

    productModel.create(newProduct,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Product Successfully !`,
                product : data
            })
        }
    })


}

//Update Product by id 
const updateProductById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productId = req.params.productId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            message :`Bad request - Product Id is not valid!`
        })
    }

    if(!body.name){
        return res.status(400).json({
            message : `Bad Request - name is required !`
        })
    }

    if(!body.imageUrl){
        return res.status(400).json({
            message : `Bad Request - imageUrl is required !`
        })
    }

    if(!body.buyPrice){
        return res.status(400).json({
            message : `Bad Request - buyPrice is required !`
        })
    }

    if(!Number(body.buyPrice)){
        return res.status(400).json({
            message : `Bad Request - buyPrice is a number !`
        })
    }

    if(!body.promotionPrice){
        return res.status(400).json({
            message : `Bad Request - promotionPrice is required !`
        })
    }

    if(!Number(body.promotionPrice)){
        return res.status(400).json({
            message : `Bad Request - promotionPrice is a number !`
        })
    }

    if(!Number(body.amount)){
        return res.status(400).json({
            message : `Bad Request - amount is number !`
        })
    }


    //B3 : Xử lý và trả về kết quả
    let updateUser = new productModel({
        name : body.name,
        description : body.description,
        imageUrl : body.imageUrl,
        buyPrice : body.buyPrice,
        promotionPrice : body.promotionPrice,
        amount : body.amount
    });

    productModel.findByIdAndUpdate(productId,updateUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}` 
            })
        }else{
            return res.status(201).json({
                message :`Update Product By Id successfully !`,
                product : data
            })
        }
    })

}

//Delete Product by id 
const deleteProductById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let productId = req.params.productId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
          message :`Internal server error : ${err.message}`  
        })
    }

    //B3 : Xử lý và trả về kết quả 
    productModel.findByIdAndDelete(productId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).json({
                message : `Delete Product Successfully !`,
                product : data
            })
        }
    })
}
//Export 
module.exports = { getAllProduct , getProductById , createNewProduct , updateProductById , deleteProductById};