//Import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Khởi tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoose
const customerSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    fullName : {
        type : String,
        required : true 
    },
    phone : {
        type : String,
        required : true,
        unique : true
    },
    email :{
        type : String,
        required : true,
        unique : true
    },
    address : {
        type : String,
        required : true 
    },
    city :{
        type : String,
        default : ""
    },
    country : {
        type : String,
        default : ""
    },
    orders :[
        {
            type : mongoose.Types.ObjectId,
            ref : 'order'
        }
    ]
})

//Export thành model
module.exports = mongoose.model("customer",customerSchema);