//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//khai báo Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoose
const orderdetailSchema = new Schema({
    product : [
        {
            type : mongoose.Types.ObjectId,
            ref : 'product'
        }
    ],
    quantity : {
        type : Number,
        default : 0
    }
},{
    timestamps : true
})

//Export Schema ra thành model
module.exports = mongoose.model("orderdetail",orderdetailSchema);