//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//khai báo Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoose
const orderSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    orderDate :{
        type : Date,
        default : Date.now()
    },
    shippedDate :{
        type : Date
    },
    note :{
        type :String
    },
    orderDetails : [
        {
            type : mongoose.Types.ObjectId,
            ref : 'orderdetail'
        }
    ],
    cost : {
        type : Number,
        default : 0
    }

},{
    timestamps : true
})

//Export Schema ra model
module.exports = mongoose.model("order",orderSchema);