//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//khai báo Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoose
const productTypeSchema = new Schema ({
    _id : mongoose.Types.ObjectId,
    name : {
        type : String,
        unique : true,
        required : true
    },
    description : {
        type : String
    }
})

//Export thành module
module.exports = mongoose.model("productType",productTypeSchema);