//Import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Khởi tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoose
const productSchema = new Schema ({
    _id : mongoose.Types.ObjectId,
    name : {
        type : String,
        unique : true,
        required : true
    },
    description : {
        type : String 
    },
    type :[
        {
            type : mongoose.Types.ObjectId,
            ref : 'productType',
        }
    ],
    imageUrl : {
        type : String,
        required : true
    },
    buyPrice : {
        type :Number,
        required : true
    },
    promotionPrice : {
        type : Number,
        required : true 
    },
    amount :{
        type : Number,
        default : 0
    }
})

//Export thành module
module.exports = mongoose.model("product",productSchema);