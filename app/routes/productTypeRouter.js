//Import thư viện express
const express = require("express");
const { getAllProductType, getProductTypeById, createNewProductType, updateProductTypeById, deleteProductTypeById } = require("../controllers/productTypeController");

//Khai báo router
const productTypeRouter = express.Router();

//Get All Product Type
productTypeRouter.get("/productType",getAllProductType);

//Get Product Type By Id
productTypeRouter.get("/productType/:productTypeId",getProductTypeById);

//Create Product Type
productTypeRouter.post("/productType",createNewProductType);

//Update Product Type
productTypeRouter.put("/productType/:productTypeId",updateProductTypeById);

//Delete Product
productTypeRouter.delete("/productType/:productTypeId",deleteProductTypeById);

//Export module
module.exports = {productTypeRouter};
