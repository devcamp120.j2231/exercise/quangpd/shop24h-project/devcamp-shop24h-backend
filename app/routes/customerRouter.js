//Import thư viện express
const express = require("express");

//Import controller
const { getAllCustomer, getCustomerById, createCustomer, updateCustomerById, deleteCustomerById } = require("../controllers/customerController");

//Khai báo router 
const customerRouter = express.Router();

//Get All Customer
customerRouter.get("/customers",getAllCustomer);

//Get Customer By id 
customerRouter.get("/customers/:customerId",getCustomerById);

//Create Customer
customerRouter.post("/customers",createCustomer);

//Update Customer
customerRouter.put("/customers/:customerId",updateCustomerById);

//Delete Customer
customerRouter.delete("/customers/:customerId",deleteCustomerById);

//Export thành module 
module.exports = {customerRouter};