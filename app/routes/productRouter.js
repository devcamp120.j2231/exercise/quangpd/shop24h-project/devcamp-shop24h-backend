//Import thư viện express
const express = require("express");
//Import Router
const { getAllProduct, getProductById, createNewProduct, updateProductById, deleteProductById } = require("../controllers/productController");

//Khai báo router
const productRouter = express.Router();

//Get All Product
productRouter.get("/products",getAllProduct);

//Get Product By Id 
productRouter.get("/products/:productId",getProductById);

//Create New Product 
productRouter.post("/products",createNewProduct);

//Update Product
productRouter.put("/products/:productId",updateProductById);

//DeleteProduct 
productRouter.delete("/products/:productId",deleteProductById);


//Export thành module 
module.exports = {productRouter};