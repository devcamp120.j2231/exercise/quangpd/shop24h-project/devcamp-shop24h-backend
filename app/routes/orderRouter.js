//Import thư viện express
const express = require("express");

//Import controller
const { getAllOrder, getOrderByid, createNewOrderOfCustomer, getAllOrderOfCustomer, updateOrderById, deleteOrderById } = require("../controllers/orderController");

//Khai báo router
const orderRouter = express.Router();

//Create New Order
orderRouter.post("/customers/:customerId/orders",createNewOrderOfCustomer);

//Get All Order
orderRouter.get("/orders",getAllOrder);

//Get OrderBy Id 
orderRouter.get("/orders/:orderId",getOrderByid);

//Update Order By OrderId 
orderRouter.put("/orders/:orderId",updateOrderById);

//Get All Order Of Customer
orderRouter.get("/customer/:customerId/orders",getAllOrderOfCustomer);

//Delete Order By Id 
orderRouter.delete("/customer/:customerId/orders/:orderId",deleteOrderById);



//Export thành modul
module.exports = {orderRouter};