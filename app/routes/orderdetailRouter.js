//Import thư viện express
const express = require("express");

//Import controller
const { getAllOrderDetail, getOrderDetailByid, createNewOrderDetailOfOrder, updateOrderDetailById, getAllOrderDetailOfOrder, deleteOrderDetailById } = require("../controllers/orderDetailController");

//Khai báo router
const orderDetailRouter = express.Router();


//Create New OrderOrderDetail
orderDetailRouter.post("/orders/:orderId/orderDetail",createNewOrderDetailOfOrder);

//Get All OrderDetail
orderDetailRouter.get("/orderDetail",getAllOrderDetail);

//Get Order Detail By Id
orderDetailRouter.get("/orderDetail/:orderDetailId",getOrderDetailByid);

//Update OrderDetail By OrderDetailId 
orderDetailRouter.put("/orderDetail/:orderDetailId",updateOrderDetailById);

//Get All OrderDetail Of Order
orderDetailRouter.get("/orders/:orderId/orderDetail",getAllOrderDetailOfOrder);

//Delete OrderDetail By Id 
orderDetailRouter.delete("/orders/:orderId/orderDetail/:orderDetailId",deleteOrderDetailById);



//Export thành module
module.exports ={ orderDetailRouter}